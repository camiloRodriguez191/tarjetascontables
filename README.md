# The Mugiwuaras

Este proyecto es una aplicación web que presenta información sobre los personajes de "Los Mugiwuaras" de One Piece. Permite seleccionar y "Ver la Recompensa" de cada personaje haciendo clic en un botón.

## Archivos Principales

- **index.html**: Página principal que muestra la interfaz de la aplicación.
- **script.js**: Contiene el código JavaScript para la lógica de la aplicación.
- **style.css**: Estilo para la apariencia visual de la aplicación.
- **JSON/theMugiwuaras.json**: Archivo JSON que contiene datos de los personajes de Los Mugiwuaras.

## Estructura del Proyecto

- **JS/app.js**: Archivo JavaScript que maneja la lógica principal de la aplicación.
- **index.html**: Página principal que muestra la interfaz de usuario.
- **style.css**: Archivo de estilo para personalizar la apariencia de la aplicación.
- **images**: Carpeta que debería contener imágenes representativas de los Mugiwuaras.

## Datos de Los Mugiwuaras

- **JSON/theMugiwuaras.json**: Este archivo JSON contiene información detallada sobre Los Mugiwuaras, incluyendo el nombre del pirata, el número de integrantes, el barco pirata, su estado activo y detalles sobre cada miembro, como el nombre, la recompensa y un identificador único.

## Uso

1. Clona este repositorio: `git clone [https://gitlab.com/camiloRodriguez191/tarjetascontables]`
2. Agrega imágenes representativas de los Mugiwuaras a la carpeta `images`.
3. Abre el archivo `index.html` en tu navegador.

¡Explora la información de Los Mugiwuaras y disfruta interactuando con la aplicación!

## Autor

[BRAYAN CAMILO RODRIGUEZ ARISMENDY]

---
