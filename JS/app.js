const item = document.getElementById("item");
const templateCard = document.getElementById("template-card").content;
const fragment = document.createDocumentFragment();

document.addEventListener("DOMContentLoaded", () => {
    fetchData();
});

const fetchData = async () => {
    try {
        const res = await fetch("./JSON/theMugiwuaras.json");
        const data = await res.json();
        console.log(data);
        PintarCartas(data);
    } catch (error) {
        console.log(error);
    }
};

const PintarCartas = data => {
    data.forEach(itemData => {
        const card = templateCard.cloneNode(true);

        // Rellenar los datos en la tarjeta
        card.querySelector('h5').textContent = itemData.name;
        card.querySelector('p').textContent = itemData.recompensa;
        card.querySelector('h1').textContent = itemData.id;
        
        // Crear una imagen y establecer el atributo src
       // const imgElement = document.createElement('img');
       // imgElement.src = itemData.img;
        // imgElement.alt = itemData.name; // Puedes establecer un atributo "alt" para accesibilidad
        // card.querySelector('.card-img-top').appendChild(imgElement);  Asegúrate de que el selector coincida con la estructura de tu tarjeta HTML.

        // Botón para seleccionar el personaje
        const botonSeleccionar = card.querySelector('.btn-primary');
        botonSeleccionar.textContent = 'Seleccionar Personaje';

        // Contador de clics para el personaje
        const contadorClics = document.createElement('span');
        contadorClics.textContent = 'Clics: 0';
        card.querySelector('.card-body').appendChild(contadorClics);

        // Manejador de clics para el botón "Seleccionar Personaje"
        let clics = 0;
        botonSeleccionar.addEventListener('click', () => {
            clics++;
            contadorClics.textContent = `Matar: ${clics}`;
        });

        // Agregar la tarjeta clonada al fragmento
        fragment.appendChild(card);
    });

    // Agregar el fragmento con las tarjetas al elemento "item"
    item.appendChild(fragment);
};

